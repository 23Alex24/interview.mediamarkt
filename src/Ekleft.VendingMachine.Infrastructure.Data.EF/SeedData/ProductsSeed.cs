﻿using Ekleft.VendingMachine.Core.Entities;
using System.Collections.Generic;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF
{
    internal class ProductsSeed
    {
        internal static IList<Product> Generate()
        {
            return new List<Product>()
            {
                new Product{Name = "Чай", Cost = 13},
                new Product{Name = "Кофе", Cost = 18},
                new Product{Name = "Кофе с молоком", Cost = 21},
                new Product{Name = "Сок", Cost = 35}
            };
        }
    }
}
