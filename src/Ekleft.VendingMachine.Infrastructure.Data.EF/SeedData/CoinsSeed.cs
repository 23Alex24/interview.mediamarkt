﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF
{
    internal class CoinsSeed
    {
        internal static IList<Coin> Generate()
        {
            return new List<Coin>()
            {
                new Coin{CoinValue = CoinValue.One},
                new Coin{CoinValue = CoinValue.Two},
                new Coin{CoinValue = CoinValue.Five},
                new Coin{CoinValue = CoinValue.Ten}
            };
        }
    }
}
