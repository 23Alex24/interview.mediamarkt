﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF
{
    internal class UserSeed
    {
        internal static User Generate(IList<Coin> coins)
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                UserCoinLinks = new List<UserCoinLink>()
                {
                    new UserCoinLink {Coin = coins[0],Count = 10 },
                    new UserCoinLink {Coin = coins[1],Count = 30 },
                    new UserCoinLink {Coin = coins[2],Count = 20 },
                    new UserCoinLink {Coin = coins[3],Count = 15 },
                }
            };

            return user;
        }
    }
}
