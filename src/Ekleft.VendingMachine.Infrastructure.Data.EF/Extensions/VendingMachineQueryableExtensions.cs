﻿using System.Linq;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions
{
    public static class VendingMachineQueryableExtensions
    {
        /// <summary>Фильтрует автоматы по id</summary>
        public static IQueryable<VendingMachineEntity> FilterById(this IQueryable<VendingMachineEntity> query, int id)
        {
            return query.Where(x => x.Id == id);
        }
    }
}
