﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Linq;

namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Extensions
{
    internal static class UserCoinsQueryableExtensions
    {
        /// <summary>фильтрует монеты пользователя по UserId</summary>
        internal static IQueryable<UserCoinLink> FilterByUserId(this IQueryable<UserCoinLink> query, Guid userId)
        {
            return query.Where(x => x.UserId == userId);
        }

        /// <summary>Фильтрует монеты пользователя по их достоинству</summary>
        /// <param name="value">CoinValue</param>
        internal static IQueryable<UserCoinLink> FilterByCoinValue(this IQueryable<UserCoinLink> query, CoinValue value)
        {
            return query.Where(x => x.Coin.CoinValue == value);
        }
    }
}
