namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VendingMachine_AddColumn_DepositedAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VendingMachines", "DepositedAmount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VendingMachines", "DepositedAmount");
        }
    }
}
