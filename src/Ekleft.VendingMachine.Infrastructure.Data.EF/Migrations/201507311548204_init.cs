namespace Ekleft.VendingMachine.Infrastructure.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Coins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CoinValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserCoinLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        CoinId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Coins", t => t.CoinId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CoinId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VendingMachineCoinLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendingMachineId = c.Int(nullable: false),
                        CoinId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Coins", t => t.CoinId)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => t.VendingMachineId)
                .Index(t => t.CoinId);
            
            CreateTable(
                "dbo.VendingMachines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VendingMachineProductLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendingMachineId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.VendingMachines", t => t.VendingMachineId)
                .Index(t => t.VendingMachineId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VendingMachineCoinLinks", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.VendingMachineProductLinks", "VendingMachineId", "dbo.VendingMachines");
            DropForeignKey("dbo.VendingMachineProductLinks", "ProductId", "dbo.Products");
            DropForeignKey("dbo.VendingMachineCoinLinks", "CoinId", "dbo.Coins");
            DropForeignKey("dbo.UserCoinLinks", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserCoinLinks", "CoinId", "dbo.Coins");
            DropIndex("dbo.VendingMachineProductLinks", new[] { "ProductId" });
            DropIndex("dbo.VendingMachineProductLinks", new[] { "VendingMachineId" });
            DropIndex("dbo.VendingMachineCoinLinks", new[] { "CoinId" });
            DropIndex("dbo.VendingMachineCoinLinks", new[] { "VendingMachineId" });
            DropIndex("dbo.UserCoinLinks", new[] { "CoinId" });
            DropIndex("dbo.UserCoinLinks", new[] { "UserId" });
            DropTable("dbo.Products");
            DropTable("dbo.VendingMachineProductLinks");
            DropTable("dbo.VendingMachines");
            DropTable("dbo.VendingMachineCoinLinks");
            DropTable("dbo.Users");
            DropTable("dbo.UserCoinLinks");
            DropTable("dbo.Coins");
        }
    }
}
