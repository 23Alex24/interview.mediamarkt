﻿using System.Collections.Generic;

namespace Ekleft.VendingMachine.UI.Model
{
    /// <summary>Модель для отображения информации о торговом автомате</summary>
    public class VendingMachineModel : IViewModel
    {
        /// <summary>Монеты автомата</summary>
        public IEnumerable<CoinSetModel> Coins { get; set; }

        /// <summary>Продукты автомата</summary>
        public IEnumerable<ProductSetModel> Products { get; set; }

        /// <summary>Внесенная сумма</summary>
        public int DepositedAmount { get; set; }
    }
}
