﻿namespace Ekleft.VendingMachine.UI.Model
{
    /// <summary>Модель Home/Index</summary>
    public class IndexModel : IViewModel
    {
        /// <summary>Информация о пользователе</summary>
        public UserModel User { get; set; }

        /// <summary>Информация о торговом автомате</summary>
        public VendingMachineModel VendingMachine { get; set; }
    }
}
