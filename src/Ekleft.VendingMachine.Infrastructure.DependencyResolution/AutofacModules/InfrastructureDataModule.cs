﻿using Autofac;
using Ekleft.VendingMachine.Core.Services;
using Ekleft.VendingMachine.Infrastructure.Data.EF;
using Ekleft.VendingMachine.Infrastructure.Data.EF.Services;
using System.Data.Entity;

namespace Ekleft.VendingMachine.Infrastructure.DependencyResolution
{
    public class InfrastructureDataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VendingMachineContext>().As<DbContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<VendingMachineService>().As<IVendingMachineService>();
        }
    }
}
