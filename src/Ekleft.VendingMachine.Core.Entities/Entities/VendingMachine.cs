﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ekleft.VendingMachine.Core.Entities
{
    [Table("VendingMachines")]
    public class VendingMachine : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        /// <summary>Внесенная сумма</summary>
        public int DepositedAmount { get; set; }

        public virtual ICollection<VendingMachineCoinLink> VendingMachineCoinLinks { get; set; }

        public virtual ICollection<VendingMachineProductLink> VendingMachineProductLinks { get; set; }
    }
}
