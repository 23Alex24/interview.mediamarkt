﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ekleft.VendingMachine.Core.Entities
{
    [Table("Coins")]
    public class Coin : IEntity
    {
        [Key]
        public int Id { get; set; }

        /// <summary>Достоинство монеты</summary>
        public CoinValue CoinValue { get; set; }

        public virtual ICollection<UserCoinLink> UserCoinLinks { get; set; }

        public virtual ICollection<VendingMachineCoinLink> VendingMachineCoinLinks { get; set; }
    }
}
