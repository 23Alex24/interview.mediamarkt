﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ekleft.VendingMachine.Core.Entities
{
    [Table("Products")]
    public class Product : IEntity
    {
        [NotMapped]
        public const int NameMaxLength = 500;

        [Key]
        public int Id { get; set; }
        
        public int Cost { get; set; }

        /// <summary>Наименование товара</summary>
        [Required]
        [MaxLength(NameMaxLength)]        
        public string Name { get; set; }

        public virtual ICollection<VendingMachineProductLink> VendingMachineProductLinks { get; set; }
    }
}
