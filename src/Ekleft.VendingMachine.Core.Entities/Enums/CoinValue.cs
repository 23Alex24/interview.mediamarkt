﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Core.Entities
{
    /// <summary>Достоинство монеты</summary>
    public enum  CoinValue
    {
        One = 1,
        Two = 2,
        Five = 5,
        Ten = 10
    }
}
