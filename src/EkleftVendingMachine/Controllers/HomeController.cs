﻿using Ekleft.VendingMachine.UI.Model;
using Ekleft.VendingMachine.UI.Services;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EkleftVendingMachine.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var bulder = DependencyResolver.Current.GetService<IModelBuilder<IndexModel, EmptyArguments>>();
            var model = await bulder.Build(EmptyArguments.DefaultInstance);
            return View(model);
        }
	}
}