﻿using Autofac;
using Autofac.Integration.Mvc;
using Ekleft.VendingMachine.Infrastructure.DependencyResolution;
using System.Reflection;
using System.Web.Mvc;

namespace EkleftVendingMachine.App_Start
{
    public class DependencyConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureDataModule());
            builder.RegisterModule(new InfrastructureMappingModule());
            builder.RegisterModule(new UIServicesModule());
            builder.RegisterModule(new BusinessModule());
            
            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}