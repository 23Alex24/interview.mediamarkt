﻿namespace Ekleft.VendingMachine.Business
{
    public sealed class CommadError
    {
        public string ErrorMessage { get; set; }
    }
}
