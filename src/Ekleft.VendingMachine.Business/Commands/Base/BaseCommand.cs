﻿using Ekleft.VendingMachine.Core.Services;
using System;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.Business
{
    public abstract class BaseCommand<TArguments> : ICommand<TArguments>
        where TArguments : ICommandArguments
    {
        private readonly IUnitOfWork _unitOfWork;

        public BaseCommand(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException();

            _unitOfWork = unitOfWork;
        }

        public virtual IUnitOfWork UnitOfWork
        {
            get
            {
                return _unitOfWork;
            }
        }

        public abstract Task<ICommandResult> Execute(TArguments arguments);

        public abstract Task<ICommandResult> Validate(TArguments arguments);
    }
}
