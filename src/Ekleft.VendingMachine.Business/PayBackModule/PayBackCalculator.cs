﻿using Ekleft.VendingMachine.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendingMachineEntity = Ekleft.VendingMachine.Core.Entities.VendingMachine;

namespace Ekleft.VendingMachine.Business
{
    public class PayBackCalculator
    {
        public Dictionary<CoinValue, int> Calculate(VendingMachineEntity machine)
        {
            //результат вычислений - сколько нужно отдать монет.
            //ключ - достоинство монеты, значение - количество
            var resultCoins = new Dictionary<CoinValue, int>();
            int deposited = machine.DepositedAmount; //сколько должны дать сдачи
            int residual = deposited; //Сколько средств еще нужно отдать

            //сначала отдаем самые крупные монеты
            var ordered = machine.VendingMachineCoinLinks
                .OrderByDescending(x => x.Coin.CoinValue)
                .ToList();

            foreach (var coin in ordered)
            {
                if (coin.Count < 1)
                    continue;

                var coinsCount = Convert.ToInt32(Math.Floor((decimal)(residual / (int)coin.Coin.CoinValue)));

                if (coinsCount == 0)
                    continue;

                if (coin.Count < coinsCount)
                    coinsCount = coin.Count;

                residual -= coinsCount * (int)coin.Coin.CoinValue;
                resultCoins.Add(coin.Coin.CoinValue, coinsCount);

                if (residual == 0)
                    break;
            }

            return resultCoins;
        }
    }
}
