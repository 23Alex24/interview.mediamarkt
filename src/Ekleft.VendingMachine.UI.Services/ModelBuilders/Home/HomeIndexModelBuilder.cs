﻿using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.Core.Services;
using Ekleft.VendingMachine.Services;
using Ekleft.VendingMachine.UI.Model;
using System;
using System.Threading.Tasks;

namespace Ekleft.VendingMachine.UI.Services
{
    public class HomeIndexModelBuilder : IModelBuilder<IndexModel, EmptyArguments>
    {
        #region Конструктор, поля

        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IVendingMachineService _machineService;

        public HomeIndexModelBuilder(IUserService userService, IVendingMachineService machineService, IMapper mapper)
        {
            if(userService ==null || machineService == null || mapper == null)
                throw new ArgumentNullException();

            _mapper = mapper;
            _userService = userService;
            _machineService = machineService;
        }

        #endregion
        

        public async Task<IndexModel> Build(EmptyArguments arguments = null)
        {
            var model = new IndexModel();

            var userIdTask = _userService.GetUserId();
            var machineIdTask = _machineService.GetMachineId();

            await Task.WhenAll(userIdTask, machineIdTask);

            var userInfoTask = _userService.GetUserInfo(userIdTask.Result);
            var machineInfoTask = _machineService.GetMachineInfo(machineIdTask.Result);

            await Task.WhenAll(userInfoTask, machineInfoTask);

            model.User = new UserModel
            {
                Id = userInfoTask.Result.Id,
                Coins = _mapper.MapEnumerable<UserCoinLink, CoinSetModel>(userInfoTask.Result.UserCoinLinks)
            };

            model.VendingMachine = new VendingMachineModel
            {
                DepositedAmount = machineInfoTask.Result.DepositedAmount,
                Coins = _mapper.MapEnumerable<VendingMachineCoinLink, CoinSetModel>(
                    machineInfoTask.Result.VendingMachineCoinLinks),
                Products = _mapper.MapEnumerable<VendingMachineProductLink, ProductSetModel>(
                    machineInfoTask.Result.VendingMachineProductLinks)
            };

            return model;
        }
    }
}
