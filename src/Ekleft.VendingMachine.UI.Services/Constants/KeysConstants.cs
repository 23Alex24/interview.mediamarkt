﻿namespace Ekleft.VendingMachine.UI.Services
{
    public static class KeysConstants
    {
        public const string SuccessMessageKey = "SuccessMessage";

        public const string ErrorMessageKey = "ErrorMessage";
    }
}
