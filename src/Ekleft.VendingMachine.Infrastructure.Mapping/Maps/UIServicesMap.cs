﻿using AutoMapper;
using Ekleft.VendingMachine.Core.Entities;
using Ekleft.VendingMachine.UI.Model;

namespace Ekleft.VendingMachine.Infrastructure.Mapping
{
    public class UIServiceMap
    {
        public static void Configure()
        {
            Mapper.CreateMap<VendingMachineCoinLink, CoinSetModel>()
                .ForMember(x => x.CoinValue, opt => opt.MapFrom(x => x.Coin.CoinValue))
                .ForMember(x => x.Count, opt => opt.MapFrom(x => x.Count));

            Mapper.CreateMap<UserCoinLink, CoinSetModel>()
                .ForMember(x => x.CoinValue, opt => opt.MapFrom(x => x.Coin.CoinValue))
                .ForMember(x => x.Count, opt => opt.MapFrom(x => x.Count));

            Mapper.CreateMap<VendingMachineProductLink, ProductSetModel>()
                .ForMember(x => x.Cost, opt => opt.MapFrom(x => x.Product.Cost))
                .ForMember(x => x.Count, opt => opt.MapFrom(x => x.Count))
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.ProductId))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Product.Name));
        }
    }
}
